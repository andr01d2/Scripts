# Scripts

Scripts to modify various environments.

## HexChat Scripts

HexChat IRC client scripts:

 - ChanFltr: Drop text to a specific channel, containing specific text  
 [hexchat/chanfltr.lua](hexchat/chanfltr.lua)

HexChat script instructions:

Install by copying the file into the directory: <HEXCHAT_CONFIG_DIR>/addons/
Then in the GUI menu: Windows -> Plugins and Scripts, click: Load...
and select the script file.

If you edit the file, go to the same dialog, and click: Reload

For hexchat scripts in lua see: https://hexchat.readthedocs.io/en/latest/script_lua.html  
For general plugin info see: https://hexchat.readthedocs.io/en/latest/plugins.html  


