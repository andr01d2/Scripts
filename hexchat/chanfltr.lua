-- SPDX-License-Identifier: MIT
hexchat.register('ChanFltr', '1.0', 'Drop text to a specific channel, containing specific text')


hexchat.hook_print('Channel Half-Operator', function (args)
-- Uncomment hexchat.print statements below to debug
    local chan = hexchat.get_info('channel')
    if hexchat.nickcmp('#VIzon', chan) == 0 then
--        hexchat.print("Got a " .. chan .. string.format(" 1/2 op from %s for %s", args[1], args[2]))
        local nick = hexchat.strip(args[2])
        if hexchat.nickcmp('ALT-F4', nick) == 0 then
--            hexchat.print("Dropped #VIzon 1/2 op for " .. nick)
            return hexchat.EAT_HEXCHAT
        end
    end
    return hexchat.EAT_NONE

end, hexchat.PRI_LOW)

